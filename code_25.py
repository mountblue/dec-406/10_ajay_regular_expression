import re

#change date formate
def change_formate(date):
    return re.sub(r'(\d{4})-(\d{1,2})-(\d{1,2})','\\3-\\1-\\2',date)


#driving code
date = "1994-12-22"
print("your date is : ",date)
print("change formate is : ",change_formate(date))