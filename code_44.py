import re

#function to ignore case
def ignore_case(string):
    pattern = re.compile('ajay',re.IGNORECASE)
    return re.sub(pattern, 'ajuuuu', string)


#driving case
string = "ajay shukla is Ajay shukla and ajaY is king"
print(ignore_case(string))