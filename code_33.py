import re

#function of selecting 5 length character
def select5(str):
	return re.findall(r'\b\w{5}\b', str)
	
	
#driving code
str = input("give your string : ")
print(select5(str))