import re


def remove_multiple_spaces(text):
    return re.sub(r'\W+', '', text)

text = """Original string: Python    Exercises                                                                            
Without extra spaces: PythonExercises"""
print(remove_multiple_spaces(text))