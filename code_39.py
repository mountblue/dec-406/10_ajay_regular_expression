import re


def remove_multiple_spaces(text):
    return re.sub(r' +', ' ', text)

text = 'ajay shukla     is the king     of     word'
print(remove_multiple_spaces(text))