import re


def snake_to_camel1(text):
    output = ''
    for x in text.split('_'):
        output = output + x.capitalize()
    return output



def snake_to_camel(word):
    return ''.join(x.capitalize() for x in word.split('_'))

text = 'python_exercises_ajay_shukla_amit_shukla_kuldeep_shukla'
print(snake_to_camel1(text))
print(snake_to_camel(text))