import re


def camel_to_snake1(text):
    str1 = re.sub('(.)([A-Z][a-z0-9]+)', r'\1_\2', text)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', str1).lower()


def camel_to_snake(text):
    return re.sub('(.)([A-Z])', r'\1_\2', text).lower()

text = 'PythonExercisesAjayShuklaAmitShuklaKuldeepShukla'
print(camel_to_snake1(text))
print(camel_to_snake(text))