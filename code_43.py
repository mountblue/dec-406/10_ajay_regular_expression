import re

#function to split the string at upper case letter
def function_break(string):
    str1 = re.sub(r'(.)([A-Z])',r'\1 \2',string)
    return str1.split(' ')


#driving code
string = "AjayShuklaIsTheKingOfWord"
print(function_break(string))