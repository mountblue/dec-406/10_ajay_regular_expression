

#replace function which simply traverse the string
# def replace(str):
#     out = ""
#     for val in str:
#         if val == " ":
#             val = "_"
#         elif val == "_":
#             val = " "
#         out += val
#     return out


# #driving code
# str = input("give your string : ")
# print(replace(str))


#replace function which simly use replace function
def replace(str):
    return str.replace(' ','1').replace('_','2').replace('1','_').replace('2',' ')

#driving code
str = input("give your string : ")
print(replace(str))