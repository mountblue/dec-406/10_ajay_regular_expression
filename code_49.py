import re


def remove_word(text):
    return re.sub(r'\W*\b\w{1,6}\b', '', text)

text = "Original string Python Exercises"
print(remove_word(text))