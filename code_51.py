import re


def space_befor_capital(text):
    return re.sub('(.)([A-Z])', r'\1 \2', text)


text = 'PythonExercisesAjayShuklaAmitShuklaKuldeepShukla'
print(space_befor_capital(text))