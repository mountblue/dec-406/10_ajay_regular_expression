import re
# Python program to remove leading zeros by Traversal and join

def removeZeros_byTraversing(ip): 
	
	new_ip = ".".join([str(int(i)) for i in ip.split(".")]) 
	return new_ip ; 


# Python program to remove leading zeros using regex 

def removeZeros_regex(ip): 
	new_ip = re.sub(r'\b0+(\d)', r'\1', ip) 
	# splits the ip by "." 
	# converts the words to integeres to remove leading removeZeros 
	# convert back the integer to string and join them back to a string 
	
	return new_ip 


ip ="100.020.003.400"
print(removeZeros_regex(ip)) 


# example2 
ip ="001.200.001.004"
print(removeZeros_regex(ip)) 

# example1 
ip ="100.020.003.400"
print(removeZeros_byTraversing(ip)) 


# example2 
ip ="001.200.001.004"
print(removeZeros_byTraversing(ip)) 
